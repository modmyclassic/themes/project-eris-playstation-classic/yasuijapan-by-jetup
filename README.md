# YasuiJapan PSC ERIS Theme (PlayStation Classic)

**By Jetup**

```
PLEASE NOTE: This theme is user submitted and not endorsed or created by ModMyClassic. All material should be free to use with third party agreement to allow the use of any third party materials. If you believe there is a conflict of interest, please contact us directly on our discord https://modmyclassic.com/discord
```

## How to use

### Project ERIS (PlayStation Classic)
1. Download the .mod package from [the Project ERIS mods page](https://modmyclassic.com/project-eris-mods/)
2. Copy the .mod package to your USB to: `USB:/project_eris/mods/`
3. Plug in and boot up the PlayStation Classic. Project ERIS will automatically install.
4. Select your custom theme from the options menu at the boot menu

## Credits

**Music Credits:**

nhhmbase 9/8

**Picture Credits:**

Takashi Yasui

## Preview

![preview](https://media.discordapp.net/attachments/533575062058434562/693564508005728306/Jap1.png)